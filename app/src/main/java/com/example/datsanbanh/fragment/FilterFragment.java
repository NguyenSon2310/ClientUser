package com.example.datsanbanh.fragment;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Spinner;

import com.example.datsanbanh.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FilterFragment extends Fragment {

    Spinner spinnerType;
    SearchView sv_YardName;
    Button btnStarOne, btnStarTwo, btnStarThree, btnStarFour, btnStarFive;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Chọn lọc");
        setEvent();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View row = inflater.inflate(R.layout.fragment_filter, container, false);
        spinnerType = (Spinner) row.findViewById(R.id.spinner_Type);
        sv_YardName = (SearchView) row.findViewById(R.id.sv_YardName);
        btnStarOne = (Button) row.findViewById(R.id.btnStarOne);
        btnStarTwo = (Button) row.findViewById(R.id.btnStarTwo);
        btnStarThree = (Button) row.findViewById(R.id.btnStarThree);
        btnStarFour = (Button) row.findViewById(R.id.btnStarFour);
        btnStarFive = (Button) row.findViewById(R.id.btnStarFive);
        return row;
    }

    private void setEvent() {
        showActionbar();
        showType();
        sv_YardName.setQueryHint("Chọn lọc theo tên sân");

        btnStarOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnStarOne.setBackgroundResource(R.drawable.star_button_select);
                btnStarTwo.setBackgroundResource(R.drawable.star_button_nonselect);
                btnStarThree.setBackgroundResource(R.drawable.star_button_nonselect);
                btnStarFour.setBackgroundResource(R.drawable.star_button_nonselect);
                btnStarFive.setBackgroundResource(R.drawable.star_button_nonselect);
            }
        });

        btnStarTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnStarOne.setBackgroundResource(R.drawable.star_button_nonselect);
                btnStarTwo.setBackgroundResource(R.drawable.star_button_select);
                btnStarThree.setBackgroundResource(R.drawable.star_button_nonselect);
                btnStarFour.setBackgroundResource(R.drawable.star_button_nonselect);
                btnStarFive.setBackgroundResource(R.drawable.star_button_nonselect);
            }
        });

        btnStarThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnStarOne.setBackgroundResource(R.drawable.star_button_nonselect);
                btnStarTwo.setBackgroundResource(R.drawable.star_button_nonselect);
                btnStarThree.setBackgroundResource(R.drawable.star_button_select);
                btnStarFour.setBackgroundResource(R.drawable.star_button_nonselect);
                btnStarFive.setBackgroundResource(R.drawable.star_button_nonselect);
            }
        });

        btnStarFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnStarOne.setBackgroundResource(R.drawable.star_button_nonselect);
                btnStarTwo.setBackgroundResource(R.drawable.star_button_nonselect);
                btnStarThree.setBackgroundResource(R.drawable.star_button_nonselect);
                btnStarFour.setBackgroundResource(R.drawable.star_button_select);
                btnStarFive.setBackgroundResource(R.drawable.star_button_nonselect);
            }
        });

        btnStarFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnStarOne.setBackgroundResource(R.drawable.star_button_nonselect);
                btnStarTwo.setBackgroundResource(R.drawable.star_button_nonselect);
                btnStarThree.setBackgroundResource(R.drawable.star_button_nonselect);
                btnStarFour.setBackgroundResource(R.drawable.star_button_nonselect);
                btnStarFive.setBackgroundResource(R.drawable.star_button_select);
            }
        });
    }

    private void showType() {
        List<String> list = new ArrayList<>();
        list.add("Giá (thấp đến cao)");
        list.add("Giá (cao đến thấp)");
        list.add("Khoảng cách");
        list.add("Sao (5 đến 0)");
        list.add("Sao (0 đến 5)");

        ArrayAdapter<String> adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spinnerType.setAdapter(adapter);
    }

    private void showActionbar() {
        // get the Toolbar from MainActivity
        final Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        // get the Actionbar from Main Activity
        final ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        // inflate the customized Action Bar view
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.fragment_actionbar, null);

        if (actionBar != null) {
            // enable the customized view and disable title
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);

            actionBar.setCustomView(v);
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.WHITE));

            //remove burger icon
            toolbar.setNavigationIcon(null);

            // add click listener to back arrow icon
            v.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().onBackPressed();
                }
            });
        }
    }
}

package com.example.datsanbanh.fragment;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.datsanbanh.R;
import com.example.datsanbanh.adapter.StadiumDetailAdapter;
import com.example.datsanbanh.model.StadiumDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailFragment extends Fragment {

    TextView txtType, txtDateTime, txtResult;
    RecyclerView mRecyclerView;
    StadiumDetailAdapter mStadiumDetailAdapter;
    List<StadiumDetail> data;
    LinearLayout ln_Filter, ln_Maps;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Kết quả");
        showActionbar();
        setEvent();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View row = inflater.inflate(R.layout.fragment_detail, container, false);
        mRecyclerView = (RecyclerView) row.findViewById(R.id.recyclerView);
        txtType = (TextView) row.findViewById(R.id.txtType);
        txtDateTime = (TextView) row.findViewById(R.id.txtDateTime);
        txtResult = (TextView) row.findViewById(R.id.txtResult);
        ln_Filter = (LinearLayout) row.findViewById(R.id.ln_Filter);
        ln_Maps = (LinearLayout) row.findViewById(R.id.ln_Map);
        return row;
    }

    private void setEvent() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            txtType.setText(bundle.getString("type"));
            String dateGet = bundle.getString("dateGet");
            String dateReturn = bundle.getString("dateReturn");
            String timeGet = bundle.getString("timeGet");
            String timeReturn = bundle.getString("timeReturn");
            if (dateGet.equals(dateReturn) == true) {
                txtDateTime.setText(timeGet + " - " + timeReturn + " Ngày " + dateGet);
            } else {
                txtDateTime.setText(timeGet + " Ngày " + dateGet + " - " + timeReturn + " Ngày " + dateReturn);
            }
            txtResult.setText("Hiển thị kết quả ở " + bundle.getString("result"));
        } else {
            Toast.makeText(getContext(), "Không nhận được dữ liệu", Toast.LENGTH_SHORT).show();
        }
        ln_Filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FilterFragment filterFragment = new FilterFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.content_frame, filterFragment);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        ln_Maps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MapsFragment mapsFragment = new MapsFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.content_frame, mapsFragment);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        showRecycler();
    }

    @SuppressLint("WrongConstant")
    private void showRecycler() {
        data = new ArrayList<>();
        data.add(new StadiumDetail(R.drawable.stadium, 3.5, "Sân đất vàng", "Quận 10", "", "", "VND 367.118/h"));
        data.add(new StadiumDetail(R.drawable.stadium, 0.0, "Sân 917A", "Quận 10", "Ưu đãi nội bộ", "VND 640.693", "VND 546.235/h"));
        data.add(new StadiumDetail(R.drawable.stadium, 4.5, "Sân Trường Hải", "Quận 10", "Ưu đãi nội bộ", "VND 332.468", "VND 283.451/h"));
        data.add(new StadiumDetail(R.drawable.stadium, 5.0, "Sân Mỹ Đình", "Hà Nội", "", "VND 332.468", "VND 283.451/h"));
        mStadiumDetailAdapter = new StadiumDetailAdapter(data);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mStadiumDetailAdapter);
    }

    private void showActionbar() {
        // get the Toolbar from MainActivity
        final Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        // get the Actionbar from Main Activity
        final ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        // inflate the customized Action Bar view
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.fragment_actionbar, null);

        if (actionBar != null) {
            // enable the customized view and disable title
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);

            actionBar.setCustomView(v);
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.WHITE));

            //remove burger icon
            toolbar.setNavigationIcon(null);

            // add click listener to back arrow icon
            v.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // reverse back the show
                    actionBar.setDisplayShowCustomEnabled(false);
                    actionBar.setDisplayShowTitleEnabled(true);
                    actionBar.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // get the drawer and DrawerToggle from MainActivity
                    // set them back as normal
                    DrawerLayout drawer = getActivity().findViewById(R.id.drawer_layout);
                    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                            getActivity(), drawer, toolbar, R.string.navigation_drawer_open,
                            R.string.navigation_drawer_close);
                    // All that to re-synchonize the Drawer State
                    toggle.syncState();
                    // Implement Back Arrow Icon
                    // so it goes back to previous Fragment
                    getActivity().onBackPressed();
                }
            });
        }
    }
}

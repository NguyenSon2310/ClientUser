package com.example.datsanbanh.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.datsanbanh.R;
import com.example.datsanbanh.adapter.StadiumDetailAdapter;
import com.example.datsanbanh.model.StadiumDetail;

import java.util.ArrayList;
import java.util.List;

public class FavouriteFragment extends Fragment {

    RecyclerView mRecyclerView;
    StadiumDetailAdapter mStadiumDetailAdapter;
    List<StadiumDetail> data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View row = inflater.inflate(R.layout.fragment_favourite, container, false);
        mRecyclerView = (RecyclerView) row.findViewById(R.id.recyclerView);
        return row;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showRecyclerView();
    }

    @SuppressLint("WrongConstant")
    private void showRecyclerView() {
        data = new ArrayList<>();
        data.add(new StadiumDetail(R.drawable.stadium, 3.5, "Sân đất vàng", "Quận 10", "", "", "VND 367.118/h"));
        data.add(new StadiumDetail(R.drawable.stadium, 0.0, "Sân 917A", "Quận 10", "Ưu đãi nội bộ", "VND 640.693", "VND 546.235/h"));
        data.add(new StadiumDetail(R.drawable.stadium, 4.5, "Sân Trường Hải", "Quận 10", "Ưu đãi nội bộ", "VND 332.468", "VND 283.451/h"));
        data.add(new StadiumDetail(R.drawable.stadium, 5.0, "Sân Mỹ Đình", "Hà Nội", "", "VND 332.468", "VND 283.451/h"));
        mStadiumDetailAdapter = new StadiumDetailAdapter(data);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mStadiumDetailAdapter);
    }

}

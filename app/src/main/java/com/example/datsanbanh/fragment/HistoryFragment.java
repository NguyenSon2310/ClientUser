package com.example.datsanbanh.fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.datsanbanh.R;
import com.example.datsanbanh.adapter.HistoryAdapter;
import com.example.datsanbanh.model.History;

import java.util.ArrayList;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {

    RecyclerView mRecyclerView;
    HistoryAdapter mRcvAdapter;
    List<History> data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View row = inflater.inflate(R.layout.fragment_history, container, false);
        mRecyclerView = (RecyclerView) row.findViewById(R.id.recyclerView);
        return row;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showHistory();
    }

    @SuppressLint("WrongConstant")
    private void showHistory() {
        data = new ArrayList<>();
        data.add(new History("Sân 917A", "09/12/2016", "Đã sử dụng", "595.000 VND", "Xong", "17h00 - 18h50, 11/12/2016"));
        data.add(new History("Sân 917A", "22/12/2016", "Chưa sử dụng", "595.000 VND", "Chưa", "17h00 - 18h50, 26/12/2016"));
        mRcvAdapter = new HistoryAdapter(data);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mRcvAdapter);
    }

}

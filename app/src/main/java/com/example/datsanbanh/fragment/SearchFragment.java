package com.example.datsanbanh.fragment;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.datsanbanh.R;
import com.example.datsanbanh.location.AppLocationService;
import com.example.datsanbanh.location.LocationAddress;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {

    TextView txtTimeGet, txtTimeReturn, txtDateGet, txtDateReturn, txtDowget, txtDowReturn;
    Spinner spinnerType, spinnerDetail;
    Button btnFind;
    LinearLayout lnDateGet, lnDateReturn;
    SearchView simpleSearchView;
    ImageButton btnLocation;
    AppLocationService appLocationService;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View row = inflater.inflate(R.layout.fragment_search, container, false);
        spinnerType = (Spinner) row.findViewById(R.id.spinnerType);
        spinnerDetail = (Spinner) row.findViewById(R.id.spinnerDetail);
        btnFind = (Button) row.findViewById(R.id.btnFind);
        txtTimeGet = (TextView) row.findViewById(R.id.txtTimeGet);
        txtTimeReturn = (TextView) row.findViewById(R.id.txtTimeReturn);
        txtDateGet = (TextView) row.findViewById(R.id.txtDateGet);
        txtDateReturn = (TextView) row.findViewById(R.id.txtDateReturn);
        txtDowget = (TextView) row.findViewById(R.id.txtDowGet);
        txtDowReturn = (TextView) row.findViewById(R.id.txtDowReturn);
        lnDateGet = (LinearLayout) row.findViewById(R.id.lnDateGet);
        lnDateReturn = (LinearLayout) row.findViewById(R.id.lnDateReturn);
        simpleSearchView = (SearchView) row.findViewById(R.id.simpleSearchView);
        btnLocation = (ImageButton) row.findViewById(R.id.btnLocation);
        return row;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Tìm kiếm");
        setEvent();
    }

    private void setEvent() {
        showStatusCurrentButton();
        appLocationService = new AppLocationService(getContext());
        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Location location = appLocationService.getLocation(LocationManager.GPS_PROVIDER);
                if (location != null) {
                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();
                    LocationAddress locationAddress = new LocationAddress();
                    locationAddress.getAddressFromLocation(latitude, longitude, getContext(), new GeocoderHandler());
                } else {
                    showSettingsAlert();
                }
            }
        });
        showType();
        showDetail();

        DateFormat df = new SimpleDateFormat("HH:mm");
        String currentTime = df.format(Calendar.getInstance().getTime());
        txtTimeGet.setText(currentTime);
        txtTimeGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickTimeGet();
            }
        });

        txtTimeReturn.setText(currentTime);
        txtTimeReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickTimeReturn();
            }
        });

        df = new SimpleDateFormat("dd/MM");
        String currentDate = df.format(Calendar.getInstance().getTime());
        txtDateGet.setText(currentDate);
        txtDateReturn.setText(currentDate);

        df = new SimpleDateFormat("EEEE");
        String currentDOW = df.format(Calendar.getInstance().getTime());
        txtDowget.setText(currentDOW);
        txtDowReturn.setText(currentDOW);

        lnDateGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickDateGet();
            }
        });

        lnDateReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickDateReturn();
            }
        });

        //Click button tìm kiếm
        btnFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String result = simpleSearchView.getQuery().toString();
                if (result.isEmpty()) {
                    Toast.makeText(getContext(), "Bạn phải nhập địa chỉ", Toast.LENGTH_SHORT).show();
                } else {
                    DetailFragment detailFragment = new DetailFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("result", result);
                    bundle.putString("timeGet", txtTimeGet.getText().toString());
                    bundle.putString("timeReturn", txtTimeReturn.getText().toString());
                    bundle.putString("dateGet", txtDateGet.getText().toString());
                    bundle.putString("dateReturn", txtDateReturn.getText().toString());
                    bundle.putString("type", spinnerDetail.getSelectedItem().toString());
                    detailFragment.setArguments(bundle);
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.content_frame, detailFragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                getContext());
        alertDialog.setTitle("SETTINGS");
        alertDialog.setMessage("Enable Location Provider! Go to settings menu?");
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        getContext().startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            simpleSearchView.setQuery(locationAddress, false);
        }
    }

    private void pickDateGet() {
        final Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                //i: năm - i1: tháng - i2: ngày
                calendar.set(i, i1, i2);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM");
                txtDateGet.setText(simpleDateFormat.format(calendar.getTime()));
                simpleDateFormat = new SimpleDateFormat("EEEE");
                txtDowget.setText(simpleDateFormat.format(calendar.getTime()));
            }
        }, year, month, day);
        datePickerDialog.show();
    }


    private void pickDateReturn() {
        final Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                //i: năm - i1: tháng - i2: ngày
                calendar.set(i, i1, i2);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM");
                txtDateReturn.setText(simpleDateFormat.format(calendar.getTime()));
                simpleDateFormat = new SimpleDateFormat("EEEE");
                txtDowReturn.setText(simpleDateFormat.format(calendar.getTime()));
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    private void pickTimeReturn() {
        final Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
                calendar.set(0, 0, 0, i, i1);
                txtTimeReturn.setText(simpleDateFormat.format(calendar.getTime()));
            }
        }, hour, min, true);
        timePickerDialog.show();
    }

    private void pickTimeGet() {
        final Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
                calendar.set(0, 0, 0, i, i1);
                txtTimeGet.setText(simpleDateFormat.format(calendar.getTime()));
            }
        }, hour, min, true);
        timePickerDialog.show();
    }


    private void showType() {
        List<String> list = new ArrayList<>();
        list.add("Sân cỏ nhân tạo");
        list.add("Sân cỏ thật");
        list.add("Sân quần vợt");
        list.add("Sân cầu lông");

        ArrayAdapter<String> adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spinnerType.setAdapter(adapter);
    }

    private void showDetail() {
        List<String> list = new ArrayList<>();
        list.add("Sân 5 người");
        list.add("Sân 7 người");
        list.add("Sân 11 người");
        ArrayAdapter<String> adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spinnerDetail.setAdapter(adapter);
    }

    private void showStatusCurrentButton() {
        simpleSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                btnFind.performClick();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if(!s.isEmpty()){
                    btnLocation.setVisibility(View.GONE);
                }else {
                    btnLocation.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });
    }
}

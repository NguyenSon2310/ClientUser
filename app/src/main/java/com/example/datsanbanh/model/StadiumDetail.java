package com.example.datsanbanh.model;

public class StadiumDetail {
    private int mIcon;
    private double mRating;
    private String mName;
    private String mState;
    private String mEndow;
    private String mSale;
    private String mPrice;

    public StadiumDetail() {
    }

    public StadiumDetail(int mIcon, double mRating, String mName, String mState, String mEndow, String mSale, String mPrice) {
        this.mIcon = mIcon;
        this.mRating = mRating;
        this.mName = mName;
        this.mState = mState;
        this.mEndow = mEndow;
        this.mSale = mSale;
        this.mPrice = mPrice;
    }

    public int getmIcon() {
        return mIcon;
    }

    public void setmIcon(int mIcon) {
        this.mIcon = mIcon;
    }

    public double getmRating() {
        return mRating;
    }

    public void setmRating(double mRating) {
        this.mRating = mRating;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmState() {
        return mState;
    }

    public void setmState(String mState) {
        this.mState = mState;
    }

    public String getmEndow() {
        return mEndow;
    }

    public void setmEndow(String mEndow) {
        this.mEndow = mEndow;
    }

    public String getmSale() {
        return mSale;
    }

    public void setmSale(String mSale) {
        this.mSale = mSale;
    }

    public String getmPrice() {
        return mPrice;
    }

    public void setmPrice(String mPrice) {
        this.mPrice = mPrice;
    }
}

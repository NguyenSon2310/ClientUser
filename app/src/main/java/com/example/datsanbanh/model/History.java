package com.example.datsanbanh.model;

public class History {
    private String name;
    private String date;
    private String status;
    private String price;
    private String pay;
    private String dateTime;

    public History() {
    }

    public History(String name, String date, String status, String price, String pay, String dateTime) {
        this.name = name;
        this.date = date;
        this.status = status;
        this.price = price;
        this.pay = pay;
        this.dateTime = dateTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPay() {
        return pay;
    }

    public void setPay(String pay) {
        this.pay = pay;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}

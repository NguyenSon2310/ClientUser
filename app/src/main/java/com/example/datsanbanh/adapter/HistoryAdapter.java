package com.example.datsanbanh.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.datsanbanh.R;
import com.example.datsanbanh.model.History;

import java.util.ArrayList;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>{
    private List<History> data = new ArrayList<>();

    public HistoryAdapter(List<History> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.listview_history_item_row, parent, false);
        return new HistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder {
        TextView txtName, txtDate, txtStatus, txtPrice, txtPay, txtDateTime;

        public HistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtStatus = (TextView) itemView.findViewById(R.id.txtStatus);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            txtPay = (TextView) itemView.findViewById(R.id.txtPay);
            txtDateTime = (TextView) itemView.findViewById(R.id.txtDateTime);
        }

        public void bind(History history) {
            txtName.setText(history.getName());
            txtStatus.setText(history.getStatus());
            txtDate.setText(history.getDate());
            txtPrice.setText(history.getPrice());
            txtPay.setText(history.getPay());
            txtDateTime.setText(history.getDateTime());
        }
    }
}

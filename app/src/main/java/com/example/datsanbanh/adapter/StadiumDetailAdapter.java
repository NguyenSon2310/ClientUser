package com.example.datsanbanh.adapter;

import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.datsanbanh.R;
import com.example.datsanbanh.fragment.BookStadiumFragment;
import com.example.datsanbanh.fragment.SearchFragment;
import com.example.datsanbanh.model.StadiumDetail;

import java.util.ArrayList;
import java.util.List;

public class StadiumDetailAdapter extends RecyclerView.Adapter<StadiumDetailAdapter.StadiumViewHolder> {

    private List<StadiumDetail> data = new ArrayList<>();

    public StadiumDetailAdapter(List<StadiumDetail> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public StadiumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.listview_item_row, parent, false);
        return new StadiumViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StadiumViewHolder holder, int position) {
        holder.bind(data.get(position));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class StadiumViewHolder extends RecyclerView.ViewHolder {
        TextView txtName, txtState, txtEndow, txtSale, txtPrice;
        ImageView imgIcon;
        RatingBar ratingBar;
        LinearLayout line;

        public StadiumViewHolder(@NonNull View itemView) {
            super(itemView);
            imgIcon = (ImageView) itemView.findViewById(R.id.imgIcon);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtState = (TextView) itemView.findViewById(R.id.txtState);
            txtEndow = (TextView) itemView.findViewById(R.id.txtEndow);
            txtSale = (TextView) itemView.findViewById(R.id.txtSale);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            ratingBar = (RatingBar) itemView.findViewById(R.id.simpleRatingBar);
            line = (LinearLayout) itemView.findViewById(R.id.line);
        }

        public void bind(StadiumDetail stadiumDetail) {
            imgIcon.setImageResource(stadiumDetail.getmIcon());
            txtName.setText(stadiumDetail.getmName());
            txtState.setText(stadiumDetail.getmState());
            txtEndow.setText(stadiumDetail.getmEndow());
            txtSale.setText(stadiumDetail.getmSale());
            txtSale.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            txtPrice.setText(stadiumDetail.getmPrice());
            ratingBar.setRating((float) stadiumDetail.getmRating());
            line.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    Bundle bundle = new Bundle();
                    bundle.putString("name", txtName.getText().toString());
                    BookStadiumFragment myFragment = new BookStadiumFragment();
                    myFragment.setArguments(bundle);
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, myFragment,"BookStadium").addToBackStack(null).commit();
                }
            });
        }
    }

    public interface OnItemClickedListener {
        void onItemClick();
    }

    private OnItemClickedListener onItemClickedListener;

    public void setOnItemClickedListener(OnItemClickedListener onItemClickedListener) {
        this.onItemClickedListener = onItemClickedListener;
    }
}
